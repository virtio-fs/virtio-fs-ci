#!/bin/bash
rm -rf /var/tmp/pjdfstest

mkdir -p /var/tmp/pjdfstest
cd /var/tmp/pjdfstest
prove -rv /root/pjdfstest/tests
rc_pjdfstest=$?

mkdir -p /var/tmp/blogbench
/root/blogbench-1.1/src/blogbench -d /var/tmp/blogbench
rc_blogbench=$?

if [ "$rc_pjdfstest" -ne 0 -o "$rc_blogbench" -ne 0 ]; then
	echo FAIL
	exit 1
else
	echo PASS
	exit 0
fi
