#!/bin/bash
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Build a Linux kernel vmlinuz
#
# Copyright (C) 2019 Red Hat, Inc.

set -e

arch=$(uname -m)

if [ ! -d include/uapi/linux ]; then
	echo 'This script must be run from a Linux source tree.'
	exit 1
fi

case ${arch} in
    x86_64)
	kernel_image=bzImage
	;;
    ppc64le)
	kernel_image=vmlinux
	defconfig=pseries_le_defconfig
	;;
esac

scriptdir=$(dirname "$0")
arch_config="$scriptdir/configs/${arch}-monolithic-virtio-fs"

if [ -f "$arch_config" ]; then
    cp "$arch_config" .config
    make oldconfig
else
    make $defconfig
    ./scripts/kconfig/merge_config.sh .config "$scriptdir/configs/virtio-fs"
    make LSMOD=$(mktemp) localyesconfig
fi

make -j$(nproc) $kernel_image
