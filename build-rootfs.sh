#!/bin/bash
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Build a root file system and produce a rootfs.tar.xz tarball
#
# Copyright (C) 2019 Red Hat, Inc.

FEDORA_RELEASE=30
FEDORA_PACKAGES="system-release vim-minimal systemd passwd dnf rootfiles autoconf automake gcc make git perl-Test-Harness tar wget"

DEBIAN_PACKAGES="vim-tiny autoconf automake gcc libc6-dev make git perl tar ca-certificates wget"

set -e

if [ $EUID != 0 ]; then
	echo 'This script must be run as root.'
	exit 1
fi

run_in_rootfs() {
	chroot "$PWD/rootfs" "$@"
}

umask 022
rm -rf rootfs
mkdir rootfs

source /etc/os-release
if [[ "$ID $ID_LIKE" =~ "fedora" ]]; then
	dnf "--installroot=$PWD/rootfs" "--releasever=$FEDORA_RELEASE" --assumeyes install $FEDORA_PACKAGES
	install -D -m 0644 /etc/resolv.conf "$PWD/rootfs/etc/"
elif [[ "$ID $ID_LIKE" =~ "debian" ]]; then
	debootstrap --include="$DEBIAN_PACKAGES" stable "$PWD/rootfs"
fi

install -D -m 0755 run-test-in-guest.sh "$PWD/rootfs/root/"
install -D -m 0644 run-test.service "$PWD/rootfs/etc/systemd/system/"
run_in_rootfs /bin/bash -c "cd /root && git clone https://github.com/pjd/pjdfstest && cd pjdfstest && autoreconf -ifs && ./configure && make pjdfstest"
run_in_rootfs /bin/bash -c "cd /root && wget -O blogbench-1.1.tar.gz https://download.pureftpd.org/pub/blogbench/blogbench-1.1.tar.gz && tar xf blogbench-1.1.tar.gz && cd blogbench-1.1 && wget -O config.guess 'https://git.savannah.gnu.org/gitweb/?p=config.git;a=blob_plain;f=config.guess;hb=HEAD' && ./configure && make"
tar cJf rootfs.tar.xz rootfs
